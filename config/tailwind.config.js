module.exports = {
  /* Use the Desy default config */
  presets: [require('desy-html/config/tailwind.config.js')],
  content: ['./node_modules/desy-html/src/**/*.html',
            './node_modules/desy-html/src/**/*.njk',
            './node_modules/desy-html/src/**/*.js',
            './src/**/*.html',
            './src/**/*.njk',
            './src/**/*.js',
            ],
  safelist: [
            'dev', // used in body tag
            'has-offcanvas-open',
            'has-dialog',
            'dialog-backdrop',
            'dialog-backdrop.active',
            'focus',
            'headroom',
            'headroom--pinned',
            'headroom--unpinned',
            'headroom--top',
            'headroom--not-top',
            'headroom--bottom',
            'headroom--not-bottom',
            'headroom--frozen'
            ],
  // extend {
  //   screens: {
  //       2xl: '1680px',
  //       3xl: '1920px',
  //   },
  // }
}
