
import autoprefixer from 'autoprefixer';
import babel from 'gulp-babel';
import browserSyncPackage from 'browser-sync';
import cache from 'gulp-cache';
import clean from 'gulp-clean';
import footer from 'gulp-footer';
import gulp from 'gulp';
import nunjucksRender from 'gulp-nunjucks-render';
import postcss from 'gulp-postcss';
import sharpOptimizeImages from 'gulp-sharp-optimize-images';
import sourcemaps from 'gulp-sourcemaps';
import stripCssComments from 'gulp-strip-css-comments';
import tailwindcss from 'tailwindcss';
import tailwindcssNesting from 'tailwindcss/nesting/index.js';
import atimport from 'postcss-import';

import { SOURCE_NUNJUCKS_FILTERS } from './node_modules/desy-html/src/js/filters/index.js';

import { series } from 'gulp';

const browserSync = browserSyncPackage.create();

const TAILWIND_CONFIG = './config/tailwind.config.js';
const SOURCE_HTML_DIR = './src/**.html';
const SOURCE_NUNJUCKS_PATHS = ['./src/templates/','./node_modules/desy-html/src/templates/'];
const SOURCE_NUNJUCKS_FILES = ['./src/templates/**/*'];
const SOURCE_NUNJUCKS_DIR = ['./src/**/*.html'];
const SOURCE_JS_FILES = ['./node_modules/desy-html/src/js/**/*.js','!./node_modules/desy-html/src/js/globals/**','!./node_modules/desy-html/src/js/filters/**','./src/js/**/*.js'];
const SOURCE_STYLESHEET = './src/css/styles.css';
const SOURCE_STYLESHEET_DIR = './src/**/*.css';
const SOURCE_IMG_DIR = './src/img/**.*';
const DESTINATION_HTML_DIR = './dist/';
const DESTINATION_JS_DIR = './dist/js';
const DESTINATION_IMG_DIR = './dist/images';
const DESTINATION_STYLESHEET = './dist/css/';

const manageEnvironment = function(environment) {
  // Custom filters
  for (const key in SOURCE_NUNJUCKS_FILTERS) {
    environment.addFilter(key, SOURCE_NUNJUCKS_FILTERS[key]);
  }
};

function bs(cb) {
  browserSync.init({
    server: {
      baseDir: './dist/',
      routes: {
        '/': './dist'
      }
    },
    host: '0.0.0.0',
    port: 3000,
    open: false,
    notify: false,
    ghostMode: false,
    middleware: function(req, res, next) {
      if (req.url === '/') {
        req.url = '/index.html';
      }
      next();
    }
  });
  cb();
}

function watchFiles(cb) {
  gulp.watch([TAILWIND_CONFIG, SOURCE_STYLESHEET_DIR], css);
  gulp.watch([SOURCE_HTML_DIR, ...SOURCE_NUNJUCKS_DIR, ...SOURCE_NUNJUCKS_FILES], series(html, nunjucks));
  gulp.watch(SOURCE_JS_FILES, js);
  gulp.watch('./dist/**/*').on('change', browserSync.reload);
  cb();
}

function css() {
  const isProd = process.env.NODE_ENV === 'production';
  let stream = gulp.src(SOURCE_STYLESHEET);

  if (!isProd) {
    stream = stream.pipe(sourcemaps.init());
  }

  stream = stream.pipe(postcss([
      atimport(),
      tailwindcssNesting(),
      tailwindcss(TAILWIND_CONFIG),
      autoprefixer()
    ]))
    .pipe(stripCssComments({preserve: false}))
    .pipe(footer('\n'));

  if (!isProd) {
    stream = stream.pipe(sourcemaps.write('.'));
  }

  return stream
    .pipe(gulp.dest(DESTINATION_STYLESHEET))
    .pipe(browserSync.stream());
}

function html() {
  return gulp.src(SOURCE_HTML_DIR)
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}

function img() {
  return gulp.src(SOURCE_IMG_DIR)
    .pipe(sharpOptimizeImages({
      jpg_to_jpg: {
        quality: 80,
        mozjpeg: true,
      },
      png_to_png: {
        quality: 80,
        palette: true
      },
    }))
    .pipe(gulp.dest(DESTINATION_IMG_DIR));
}

function imgDev() {
  return gulp.src(SOURCE_IMG_DIR)
    .pipe(gulp.dest(DESTINATION_IMG_DIR));
}

function nunjucks() {
  return gulp.src(SOURCE_NUNJUCKS_DIR)
    .pipe(nunjucksRender({
      envOptions: {
        autoescape: true,
        trimBlocks: true,
        lstripBlocks: true,
        noCache: true
      },
      manageEnv: manageEnvironment,
      path: SOURCE_NUNJUCKS_PATHS
    }))
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}

function js() {
  const isProd = process.env.NODE_ENV === 'production';
  let stream = gulp.src(SOURCE_JS_FILES);

  if (!isProd) {
    stream = stream.pipe(sourcemaps.init());
  }

  stream = stream.pipe(babel());

  if (!isProd) {
    stream = stream.pipe(sourcemaps.write('.'));
  }

  return stream.pipe(gulp.dest(DESTINATION_JS_DIR));
}

function assets() {
  return gulp.src(['./package.json', './src/EUPL-1.2.txt'])
    .pipe(gulp.dest('./dist/'));
}

function cleanFolder() {
  return gulp.src(DESTINATION_HTML_DIR, {read: false, allowEmpty: true})
    .pipe(clean());
}

export default series(
  cleanFolder,
  series(html, imgDev, nunjucks, js, css, assets),
  bs,
  watchFiles
);

export const prod = series(
  cleanFolder,
  series(html, img, nunjucks, js, css, assets),
  bs
);
